package main

import (
	"bitbucket.org/mrd0ll4r/tbotapi"
	"bitbucket.org/mrd0ll4r/tbotapi/examples/boilerplate"
	"fmt"
	"time"
)

func main() {
	apiToken := "123456789:Your_API_token_goes_here"

	updateFunc := func(update tbotapi.Update, api *tbotapi.TelegramBotAPI) {
		switch update.Type() {
		case tbotapi.MessageUpdate:
			msg := update.Message
			typ := msg.Type()
			if typ.IsChatAction() {
				//ignore chat actions, like group creates or joins
				fmt.Println("Ignoring chat action")
				return
			}
			if msg.Chat.IsChannel() {
				//ignore messages _about_ channels (bots cannot receive from channels, but will probably get events like channel creations)
				fmt.Println("Ignoring channel message")
				return
			}

			// display the incoming message
			// msg.Chat implements fmt.Stringer, so it'll display nicely
			// MessageType implements fmt.Stringer, so it'll display nicely
			fmt.Printf("<-%d, From:\t%s, Type: %s \n", msg.ID, msg.Chat, typ)

			// create a message with some keyboard markup
			toSend := api.NewOutgoingMessage(tbotapi.NewRecipientFromChat(msg.Chat), "What time is it where I am?")
			toSend.SetReplyKeyboardMarkup(tbotapi.ReplyKeyboardMarkup{
				Keyboard:        [][]string{[]string{time.Now().Format(time.RFC1123Z)}},
				OneTimeKeyboard: true,
			})

			// send it
			outMsg, err := toSend.Send()

			if err != nil {
				fmt.Printf("Error sending: %s\n", err)
				return
			}
			fmt.Printf("->%d, To:\t%s, Text: %s\n", outMsg.Message.ID, outMsg.Message.Chat, *outMsg.Message.Text)
		case tbotapi.InlineQueryUpdate:
			fmt.Println("Ignoring received inline query: ", update.InlineQuery.Query)
		case tbotapi.ChosenInlineResultUpdate:
			fmt.Println("Ignoring chosen inline query result (ID): ", update.ChosenInlineResult.ID)
		default:
			fmt.Printf("Ignoring unknown Update type.")
		}
	}

	// run the bot, this will block
	boilerplate.RunBot(apiToken, updateFunc, "KeyboardMarkup", "Demonstrates keyboard markup")
}
